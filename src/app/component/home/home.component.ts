import { Component, OnDestroy, OnInit } from '@angular/core';
import { Employee } from '../../model/employee';
import { SharedDataService, globalEventName } from '../../service/share.service';
import { Options, ChangeContext } from 'ng5-slider';

@Component({
	selector: 'app-home',
	templateUrl: './home.html'
})
export class HomeComponent implements OnInit, OnDestroy {

	// Employee active when user selected on left menu
	employee: Employee;
	// Config for slider
	valueSlider: number = 1;
	options: Options = {
		floor: 0,
		ceil: 100,
		step: 1,
		hideLimitLabels: true,
		hidePointerLabels: true
	};

	constructor(
		private _sharedDataService: SharedDataService
	) { }

	ngOnInit() {
		this._sharedDataService.getObservable(globalEventName.employee).subscribe((employee: Employee) => {
			this.employee = employee;
		});
	}

	/**
	 * Handle event slider change and update to another view
	 * For example: If user stay in employee A and scroll slider, we'll update font-size of title & menu of employee A
	 * @param changeContext: ChangeContext
	 */
	changeSlider(changeContext: ChangeContext) {
		this.employee.popularity = changeContext.value;
		this._sharedDataService.setObservable(globalEventName.popularity, this.employee.popularity);
	}

	/**
	 * Need to unsubscribe all observableState key registry before.
	 */
	ngOnDestroy() {
		this._sharedDataService.unsubscribe(globalEventName.employee);
	}
}
