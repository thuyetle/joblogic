import { Component, OnInit } from '@angular/core';
import { EmployeeService } from './service/employee.service';
import { Employee } from './model/employee';
import { HttpErrorResponse } from '@angular/common/http';
import { SharedDataService, globalEventName } from './service/share.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.html'
})
export class AppComponent implements OnInit {

	employeeActive: Employee;

	employees: Employee[];

	constructor(
		private _sharedDataService: SharedDataService,
		private _httpService: EmployeeService
	) {
		this.setHeightContent();
	}

	ngOnInit() {
		this.getEmployeeDataJSON();
		this._sharedDataService.getObservable(globalEventName.popularity).subscribe((popularity) => {
			if (this.employees) {
				this.employees.forEach(x => {
					if (this.employeeActive.name === x.name) {
						x.popularity = popularity;
					}
				})

			}
		});

	}

	/**
	 * Handle when user switch to another employee
	 * For example: when user in employee A and swith to employee B we need clear employee A in Observable and store the data of employee B as Observable
	 * @param employee: Employee
	 */
	switchEmployee(employee: Employee) {
		this.employeeActive = employee;
		this._sharedDataService.setObservable(globalEventName.employee, this.employeeActive);
	}

	/**
	 * Read data from JSON file
	 */
	private getEmployeeDataJSON() {
		this._httpService.getEmployeeDataJSON().subscribe(
			data => {
				if (data && data.employees) {
					this.employees = data.employees as Employee[];
					// Set default first employee
					this.employeeActive = this.employees[0];
					this._sharedDataService.setObservable(globalEventName.employee, this.employeeActive);
				}
			},
			(err: HttpErrorResponse) => {
				console.log(err.message);
			}
		);
	}

	/**
	 * Calculate height of content for make sure it's full 100%
	 */
	private setHeightContent() {
		setTimeout(function () {
			let headerHeight = document.getElementById("header") ? document.getElementById("header").clientHeight : 0;
			let bodyHeight = document.getElementsByTagName("html").length ? document.getElementsByTagName("html")[0].clientHeight : 0;
			if (document.getElementById('content')) {
				document.getElementById('content').style.height = (bodyHeight - headerHeight) + 'px';
			}
		}, 100);
	}
}
