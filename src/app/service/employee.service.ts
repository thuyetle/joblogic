import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from "rxjs/operators";
import { Observable, throwError } from 'rxjs';

@Injectable()
export class EmployeeService {
	private url: string = "./assets/data/employee.json";

	constructor(private http: HttpClient) { }

	getEmployeeDataJSON(): Observable<any> {
		return this.http.get(this.url);
	}
}
