import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { HttpClientModule } from '@angular/common/http';
import { Ng5SliderModule } from 'ng5-slider';
import { EmployeeService } from './service/employee.service';
import { SharedDataService } from './service/share.service';

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
	],
	imports: [
		BrowserModule.withServerTransition({ appId: 'my-app' }),
		RouterModule.forRoot([
			{ path: '', component: HomeComponent, pathMatch: 'full' }
		]),
		Ng5SliderModule,
		TransferHttpCacheModule,
		HttpClientModule
	],
	providers: [
		EmployeeService,
		SharedDataService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
