### Installation
* `npm install`

### Development (Client-side only rendering)
* run `npm run start` which will start `ng serve`

### Production 
**`npm run build:prerender && npm run serve:prerender`** - You can view it on `http://localhost:8080`
